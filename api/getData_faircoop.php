<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function curl($url){

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url );
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

  $data = curl_exec($ch);
  curl_close($ch);

  return $data;

}
$fairFairoRate = json_decode(curl("https://fairo.exchange/fair.json"))->FAIRO->last;
$fairEurRate = json_decode(curl("https://getfaircoin.net/api/ticker"))->EUR->last;
$fiatData = json_decode(curl("https://api.exchangeratesapi.io/latest"));

if(is_numeric($fairFairoRate) && is_numeric($fairEurRate) && is_numeric($fiatData->rates->USD)){
	$output = array("EUR" => (float)number_format($fairEurRate,3), "FAIRO" => (float)number_format($fairFairoRate,3));
	foreach($fiatData->rates as $currencySymbol=>$currencyRate){
		$output[$currencySymbol] = (float)number_format($currencyRate*$fairEurRate,3);
	}
	header('Content-type: text/javascript');
	$fp = fopen('/var/www/faircoin.co/api/faircoop.json', 'w');
	fwrite($fp, json_encode($output));
	fclose($fp);
	exit(json_encode($output));

}

exit("Unable to get data!");

?>