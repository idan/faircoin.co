[whitepaper.pdf](/docs/whitepaper_de.pdf){: .btn .btn-sm .btn-cover}
{: .download_pdf }

# FairCoin FreeVision Grundsatzpapier

## Einleitung
FairCoin existiert seit 2014 mit der Vision eine fair und kooperativ funktionierende Ökonomie zu fördern
Alle Informationen findet ihr unter https://faircoin.world der Basis-Webseite von FairCoin.

Schwerpunkt von FairCoin ist der Preismechanismus zur Stablisierung des Tauschkurses sowie der Vorbeugung von hochvolatilen Spekulationen.

Ein weiterer Schwerpunkt ist der Proof-of-Cooperation Algorithmus in dem spezifische Netzknotenpunkte in einem Rundlauf-Verfahren ( Round Robin ) neue Blöcke ( in denen sich die Transaktionen befinden ) generieren. Die Transaktionsgebühr ist dabei auf ein Minimum festgesetzt und spielt selbst beim Transfer von Kleinstbeträgen keine Rolle. Dieser Proof-of_Cooperation Algorithmus spart Ressourcen und skaliert zudem im Vergleich zu anderen Algorithmen wie Proof-of-Work oder Proof-of-Stake deutlich besser.

Die aktuelle Situation zeigt uns, dass der existierende Preismechanismus ( die strikte Festsetzung auf 1.20€ / FairCoin ) äußerst limitierend wirkt und den Bedürfnissen vieler Teilnehmer, vor allem Händler, nicht entsprechen kann.
Das Preissystem ist zudem stark abhängig von neuer Liquidität, hierbei vor allem größere Investoren, welche notwendig sind um den Preis auf dieses strikte Level stabil und liquide halten zu können.
Während der Erstellung dieses Grundsatzpapiers liegt der Marktpreis bei nur 0.10€.

FairCoin FreeVision möchte diesen Preismechanismus nicht in Frage stellen, jedoch einen anderen Mechanismus hinzufügen welcher nicht von Investoren abhängig ist und zu jedem Zeitpunkt unabhängig der Marktsituation funktioniert.

Wir wollen weitere Perspektiven für FairCoin schaffen, streben jedoch in keinster Weise eine Fork der FairCoin Blockchain an.
Wir möchten auch nicht konkurrieren sondern wollen das gesamte FairCoin Ökosystem mit unseren Ideen und Lösungen verbessern.

Wir sehen FairCoin aktuell hauptsächlich von Fair.Coop verwaltet, einer Bewegung/Organisation mit dem Ziel eine alternative Ökonomie aufzubauen.
Komun.org einer weiteren Bewegung/Organisation welche ähnliche Ziele wie Fair.Coop verfolgt, den Aspekt Dezentralität höher priorisiert.

Wir denken wenn wir mit FairCoin eine starke Digitalwährung erschaffen wollen, wir FairCoin in eine möglichst breite Masse streuen müssen. Dabei sollten wir auch jene Menschen einladen, welche an einer fairen kooperierenden Ökonomie interessiert sind, jedoch einen weniger idealistischen und eher pragmatistischen Weg wählen.
Wir sind bereit Kompromisse einzugehen wenn sie uns einen weiteren Schritt vorwärts bringen können.

FairCoin FreeVision soll als ein Teil der FairCoin community verstanden werden, welcher den freien Markt weder ignorieren, noch verteufeln möchte, stattdessen den freien Markt nutzen möchte um Liquidität zu schaffen. Wir streben einen Mittelweg zwischen Liquidität und Stabilität an, Dezentralität ( technisch wie organisatorisch ) ist uns dabei ein großes Anliegen.

FairCoin FreeVision ist gemacht für Menschen, welche ihren Fokus auf Freiheit und Nachhaltigkeit setzen.

Unsere Vision ist es FairCoin zu einem liquiden Standard für lokale sowie soziale Währungen zu machen und eine globale standardisierte Infrastruktur für den Zahlungsverkehr zu erschaffen.


## Der Preismechanismus

Preisanpassungen sollten unserer Ansicht nach nicht von Menschen in Versammlungen beschlossen werden. Wir denken, dass eine relativ kleine Masse an Menschen in einer Versammlung nicht über eine globale Gemeinde mit einer Vielzahl an Mitgliedern entscheiden sollte.

Wir denken, dass die Preisanpassungen in FairCoin über einen Automatismus, unter Berücksichtigung des freien Marktes, erfolgen sollten und die bisherige Preisfixierungen in andere Digitalwährungen wie FairCash oder soziale Währungen ausgelagert werden sollte.

FairCoin als liquides Tausch- und Zahlungsmittel.
FairCash als sehr wertstabiles Mittel der Wertsicherung.

### Warum bevorzugen wir einen Preismechanismus basierend auf den Kursen des freien Marktes statt einen Preis einfach festzulegen?

Weil ...

* unser Preis in stufenweise angepasst wird und daher dennoch ein hohes Maß an Stabilität schafft.

* Wir nutzen hierfür im Mittel 25%ige Schrittweiten, welche betragsmäßig normalisiert wurden um krumme Umrechnungskurse zu vermeiden. ( FreeVision bid ) Dieser Kurs dient gleichzeitig als Referenz z.B. für den Tausch von Waren und Gütern.

* Für den Tausch in FairCoin nutzen wir eine Schrittweite von durchschnittlich 5%. ( FreeVision ask )

* Um zu verhindern, dass durch Kurssprünge am freien Markt ein ständiger Wechsel des FreeVision Tauschkurses stattfindet, verwenden wir eine Hysterese von 5% welche diesen Effekt verhindern soll.

* Zudem erfolgt eine Anhebung des FreeVision Preises nur über die Steigerung des "bid" Preises am freien Markt. Spekulative Käufe führen nur dann zu Kurssteigerungen wenn die Kurse auf Seiten der Kaufaufträge ebenfalls steigen.

* Eine Absenkung des FreeVision Preises erfolgt nur über einen sinkenden Kurs auf Seiten der Verkaufaufträge.

* Darüber hinaus verwenden wir eine Auftragstiefe von 10000 FairCoin um Kursbewegungen durch Aufträge mit geringer Wertigkeit zu verhindern. ( Anpassungen der Auftragstiefe in Zukunft ist angedacht )  

```
define('LOG_NORMALIZED_25',
  Array(
    1,
    1.25,
    1.5,
    1.75,
    2,
    2.5,
    3,
    3.5,
    4,
    5,
    6,
    7,
    8
  )
);

define('LOG_NORMALIZED_5',
  Array(
    1,
    1.05,
    1.1,
    1.15,
    1.2,
    1.25,
    1.3,
    1.35,
    1.4,
    1.5,
    1.6,
    1.7,
    1.8,
    1.9,
    2,
    2.1,
    2.2,
    2.3,
    2.4,
    2.5,
    2.6,
    2.7,
    2.8,
    3,
    3.25,
    3.5,
    3.75,
    4,
    4.25,
    4.5,
    4.75,
    5,
    5.25,
    5.5,
    6,
    6.5,
    7,
    7.5,
    8,
    8.5,
    9,
    9.5
    )
  );
```

Der FreeVision "bid" Preis ist zu jederzeit niedriger als der "bid" Preis des freien Marktes.
Der FreeVision "ask" Preis ist zu jederzeit höher als der "ask" Preis des freien Marktes. ( "bid" => Kaufaufträge, "ask" => Verkaufsaufträge )

Arbitrage-Spekulationen können demzufolge vorgebeut und stets ausreichend Liquidität sichergestellt werden.


## Das Tausch-System

Grundsätzlich tauschen wir andere Währungen zu FairCoin zu Marktpreisen und FairCoin zu anderen Währungen zu unserem FreeVision "bid" Preis.

Der daraus resultierende Arbitrage-Gewinn werden wir für den Erhalt und Weiterentwicklung des Systems verwenden.

Unser Tausch-System wird offen und transparent für Jedermann sein ( unter Berücksichtigung der Privatsphäre der Nutzer ) und über P2P Tauschplattformen realisiert werden.

### Das Slot-System

Um den Tausch möglichst komfortabel und effizient zu gestalten verwenden wir ein Slot-System.

Jeder Slot hat eine vordefinierte Größe von aktuell 1000 FairCoin. Ein Umtausch wird nur in einem Vielfachen dieser Größenordnung unterstützt.

Die maximale Anzahl an Slots ist auf aktuell 10 begrenzt, d.h. maximal 10 * 1000 FairCoin = 10.000 FairCoin. Dies erachten wir als notwendig um der Dezentralität und den Herausforderungen eines P2P Tauschs gerecht werden zu können.

Da der FreeVision Preismechanismus äußerlich keine Arbitrage-Spekulationen zulässt, bestehen für die unterstützenden Peers zu keinem Zeitpunkt Preisrisiken.

Um zu erschweren, dass Spekulanten von Außen in das Tauschsystem eindringen und Arbitrage-Gewinne abschöpfen, setzen wir auf vertrauenswürdig-gekennzeichnete Peers, sowie zukünftig auf ein implementiertes Web-of-Trust.
