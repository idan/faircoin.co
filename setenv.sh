#!/bin/bash
IFS='/' read -ra arr <<< `dirname "$(readlink -f "$0")"`
export CI_PROJECT_NAME=${arr[-1]}
export RUNNER_UID=`id -u`
export RUNNER_GID=`id -g`
export `cat ./env/*`
