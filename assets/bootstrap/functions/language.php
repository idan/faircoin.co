<?php

define('LN_default','en');

$LANG = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
if( !empty($_GET['ln']) ) $LANG=$_GET['ln'];

$fn='languages/lang_'.$LANG.'_freevision.json';
if( !file_exists($fn) ) $LANG=LN_default;
$fn='languages/lang_'.$LANG.'_freevision.json';

$fp=fopen($fn,'r');
define('LN',json_decode(fread($fp,filesize($fn)),true));
fclose($fp);

function ln($k){
  if( !empty(LN[$k]) ){ return LN[$k]; } else { return $k; }
}

?>
