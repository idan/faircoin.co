<?php

if( empty($_GET['addr'] ) ) exit;

$address=$_GET['addr'];

define('ELECTRUMFAIR_USER',getenv('erpcuser'));
define('ELECTRUMFAIR_PASS',getenv('erpcpassword'));
define('ELECTRUMFAIR_PORT',getenv('erpcport'));
define('ELECTRUMFAIR_RPC','http://'.ELECTRUMFAIR_USER.':'.ELECTRUMFAIR_PASS.'@127.0.0.1:'.ELECTRUMFAIR_PORT);

define('FAIRCOIND_USER',getenv('rpcuser'));
define('FAIRCOIND_PASS',getenv('rpcpassword'));
define('FAIRCOIND_PORT',getenv('rpcport'));

define('FAIRCOIND_RPC', 'http://'.FAIRCOIND_USER.':'.FAIRCOIND_PASS.'@127.0.0.1:'.FAIRCOIND_PORT);

if( empty(ELECTRUMFAIR_USER) ||
    empty(ELECTRUMFAIR_PASS) ||
    empty(ELECTRUMFAIR_PORT) ||
    empty(FAIRCOIND_USER) ||
    empty(FAIRCOIND_PASS) ||
    empty(FAIRCOIND_PORT) ) exit;

/**
 *
 */

class FaircoinAddress
{
  public $param=Array( 'jsonrpc' => '2.0', 'id' => 'curltext' );
  public $address='';
  public $TX=Array();
  public $online=false;
  public $result=false;

  function __construct($address){
    $this->address=$address;
    $this->online = !empty( $this->getservers() );
  }

  function getservers(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array();
    $result=json_decode( $this->electrumfair(), true )['result'];
    return $result;
  }

  function getblockhash($index){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( $index );
    return json_decode( $this->faircoind(), true )['result'];
  }

  function getblock($blockhash){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( $blockhash );
    return json_decode( $this->faircoind(), true )['result'];
  }

  function getaddresshistory(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'address' => $this->address );
    $result=json_decode( $this->electrumfair(), true )['result'];

    $this->TX=Array();

    foreach( $result as $r ){
      $tx=$this->gettransaction( $r['tx_hash'] );
      $this->TX[ $r['tx_hash'] ]['addr'] = $tx;
    }

    return $result;

  }

  function gettransactions($txhash, $is_input=false ){

    $result=$this->gettransaction($txhash,$is_input);

    $TXS=Array( 'inputs' => [], 'outputs' => [] );

    if( $is_input == false ){
      ## check inputs by outputs of previous tx
      $txin=$result['inputs'];
      foreach( $txin as $t ){
        $TXS['inputs'][$t['address']]=$this->gettransaction( $t['prevout_hash'], true )['outputs'][$t['prevout_n']]['value'];
      }
    }

    $txout=$result['outputs'];
    foreach( $txout as $t ){
      ## check outputs of tx
      $TXS['outputs'][$t['address']]=$TXS[$t['address']] + $t['value'];
    }

    return $TXS;

  }

  function gettransaction($txhash, $is_input=false ){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'txid' => $txhash );
    $result=json_decode( $this->electrumfair(), true );
    $result=json_decode( $this->deserialize( $result['result']['hex'] ), true )['result'];
    return $result;
  }

  function deserialize($hex){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'tx' => $hex );
    return $this->electrumfair();
  }

  function getaddressbalance(){
    $this->param['method']=__FUNCTION__;
    $this->param['params']=Array( 'address' => $this->address );
    return json_decode( $this->electrumfair(), true )['result'];
  }

  function electrumfair(){ return $this->rpccall(ELECTRUMFAIR_RPC); }
  function faircoind(){ return $this->rpccall(FAIRCOIND_RPC); }

  function rpccall($url){

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST,true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $this->param ) );

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;

  }

}


$F=new FairCoinAddress($address);
$F->getaddresshistory();

$incoming_tx=array_map(
   function($v){
     return array_map(
       function($w){
         return $w['address'] == $_GET['addr'] ? 1 : 0;
       }, $v['addr']['outputs'] );
  }, $F->TX );
  
$incoming_value=array_map(
   function($v){
     return array_map(
       function($w){
         return $w['address'] == $_GET['addr'] ? $w['value'] : 0;
       }, $v['addr']['outputs'] );
  }, $F->TX );

$incoming_tx=array_map( function($v) { return array_sum($v); } , $incoming_tx);
$incoming_tx=array_sum($incoming_tx);
$incoming_value=array_map( function($v) { return array_sum($v); } , $incoming_value);
$incoming_value=array_sum($incoming_value);

$A=array(
     'incoming_tx' => $incoming_tx,
     //'balance' => $F->getaddressbalance()['confirmed']
	 'balance' => $incoming_value/100000000
   );

echo json_encode($A);

?>
