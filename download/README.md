# Developers Guide

## File Verify & Sign


## Build wallets binaries

### ElectrumFair Wallet

#### Linux

###### Create Linux Binaries

check Python3 version

```
python3 -V
```
If python3.7+ is NOT installed then download latest version at python.org
Extract the files and build python3:

```
./configure
make
make test
sudo make install
```

clone git ( release selection by ```-b branch``` )

```
mkdir ~/faircoin_dev
cd ~/faircoin_dev
git clone -b 3.3.4-fc https://github.com/criptomart/electrumfair.git
```


```
contrib/make_locale
contrib/make_packages
contrib/make_tgz
```
binaries created
```
dist/ElectrumFair-x.x.x.tar.gz
dist/ElectrumFair-x.x.x.zip
```

sign binaries
```
gpg2 --detach-sign --local-user tonyford -a ElectrumFair-*.tar.gz
```
( without ```--local-user username``` the default key of your local machine will used )

verify signature
```
gpg2 --verify ElectrumFair-*.tar.gz.asc
```

###### Create Linux AppImage

install docker

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce
```

build image

```
sudo docker build --no-cache -t electrum-appimage-builder-img contrib/build-linux/appimage
```

build binary

```
sudo docker run -it \
  --name electrum-appimage-builder-cont \
  -v $PWD:/opt/electrumfair \
  --rm \
  --workdir /opt/electrumfair/contrib/build-linux/appimage \
  electrum-appimage-builder-img \
  ./build.sh $(python3 -c "import electrum.version; print(electrum.version.ELECTRUMFAIR_VERSION)")
```

The generated binary is in `./dist`.

sign binaries
```
gpg2 --detach-sign --local-user tonyford -a ElectrumFair-*.AppImage
```
( without ```--local-user username``` the default key of your local machine will used )

verify signature
```
gpg2 --verify ElectrumFair-*.AppImage.asc
```



**get directory of packages**
```
dpkg -L <package_name>
```

**list all docker images**
```
sudo docker images
```

**access to docker image**
```
docker run -i -t --rm electrum-appimage-builder-cont /bin/bash
```


#### Android

Detailed instructions :
```
electrum/gui/kivy/Readme.md
```
Short instructions:

1. Install Docker

    ```
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt-get update
    sudo apt-get install -y docker-ce
    ```

2. Build image

    ```
    sudo docker build -t electrum-android-builder-img electrum/gui/kivy/tools
    ```

3. Build locale files

    ```
    contrib/make_locale
    ```

4. Prepare pure python dependencies

    ```
    contrib/make_packages
    ```

5. Build binaries

    ```
    sudo docker run -it --rm \
        --name electrum-android-builder-cont \
        -v $PWD:/home/user/wspace/electrum \
        -v ~/.keystore:/home/user/.keystore \
        --workdir /home/user/wspace/electrum \
        electrum-android-builder-img \
        ./contrib/make_apk
    ```
    This mounts the project dir inside the container,
    and so the modifications will affect it, e.g. `.buildozer` folder
    will be created.

5. The generated binary is in `./bin`.


**Clear cache before rebuild**

You probably need to clear the cache:
```
rm -rf .buildozer/android/platform/build/{build,dists}
```
**Quick testing by connected phone**

Assuming `adb` is installed ( `sudo apt-get install android-tools-adb`)
```
adb -d install -r bin/ElectrumFair-*-debug.apk
adb shell monkey -p org.electrumfair.electrumfair 1
```

**Getting shell access inside Docker**
```
sudo docker run -it --rm \
    -v $PWD:/home/user/wspace/electrum \
    --workdir /home/user/wspace/electrum \
    electrum-android-builder-img
```


**How do I get more verbose logs?**

See `log_level` in `buildozer.spec`

**Getting access to phone system logs**
```
adb logcat [--help]
```
